import PropTypes from 'prop-types';

export default PropTypes.shape({
  community: PropTypes.string.isRequired,
  thread: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  author: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]).isRequired,
  reactions: PropTypes.array,
});
