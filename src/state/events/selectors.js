import { createSelector } from 'reselect';

import * as authSelectors from 'state/auth/selectors';

// Input selectors
const selectData = state => state.events.data;
//

export const selectAllIdsWithAssistance = createSelector(
  selectData,
  data => Object.keys(data).filter(id => data[id].features.assistance),
);

const selectById = createSelector(
  selectData,
  (_, id) => id,
  (data, eventId) => data[eventId],
);

export const isLoaded = createSelector(
  selectById,
  event => !!event,
);

export const selectName = createSelector(
  selectById,
  event => (event ? event.name : null),
);

export const selectFlyer = createSelector(
  selectById,
  event => (event ? event.cover : null),
);

export const selectAddressString = createSelector(
  selectById,
  (event) => {
    if (!event) return null;

    const { address } = event;
    if (!address.streetNumber) {
      if (!address.intersection) return `${address.adminArea}, ${address.country}`;
      return `${address.intersection}, ${address.adminArea}, ${address.country}`;
    }
    return `${address.route} ${address.streetNumber}, ${address.adminArea}, ${address.country}`;
  },
);

export const selectInstructions = createSelector(
  selectById,
  event => (event ? event.instructions : null),
);

const selectLatLng = createSelector(
  selectById,
  event => (
    (!event || !event.location || !event.location.coordinate) ? null : event.location.coordinates
  ),
);

export const selectLat = createSelector(
  selectLatLng,
  coordinates => (coordinates ? coordinates[0] : null),
);

export const selectLng = createSelector(
  selectLatLng,
  coordinates => (coordinates ? coordinates[1] : null),
);

export const selectDate = createSelector(
  selectById,
  event => (event ? event.date : null),
);

export const isPublic = createSelector(
  selectById,
  event => (event ? event.isPublic : false),
);

export const selectPrice = createSelector(
  selectById,
  event => (event ? event.price : null),
);

export const selectCurrency = createSelector(
  selectById,
  event => (event ? event.currency : null),
);

export const selectRsvpCount = createSelector(
  selectById,
  event => (event ? event.rsvpCount : null),
);

export const selectRsvps = createSelector(
  selectById,
  event => ((event && event.rsvps) ? event.rsvps : []),
);

export const selectRsvpUserIds = createSelector(
  selectRsvps,
  rsvps => rsvps.map(r => r.userId),
);

export const selectBans = createSelector(
  selectById,
  event => ((event && event.bans) ? event.bans : []),
);

const selectOrganizers = createSelector(
  selectById,
  event => ((event && event.organizers) ? event.organizers : []),
);

export const selectOrganizerIds = createSelector(
  selectOrganizers,
  organizers => organizers.map(r => r.userId),
);

export const isOrganizer = createSelector(
  selectOrganizers,
  authSelectors.selectId,
  (organizers, meId) => meId && organizers.some(o => o.userId === meId),
);

export const hasRSVPd = createSelector(
  selectById,
  event => (event ? event.rsvpd : false),
);

export const selectRsvpLimit = createSelector(
  selectById,
  event => ((event && event.rsvpLimit) ? event.rsvpLimit : null),
);

export const hasAssistanceFeatureEnabled = createSelector(
  selectById,
  event => ((!event || !event.features) ? false : event.features.assistance),
);

export const selectQR = createSelector(
  selectById,
  event => (event ? event.qr : null),
);

export const selectSponsors = createSelector(
  selectById,
  event => (event ? event.sponsors : null),
);

export const selectLinks = createSelector(
  selectById,
  event => (event ? event.links : []),
);

export const selectAnnouncementLink = createSelector(
  selectLinks,
  links => links[0],
);

export const selectPreferenceInitPoint = createSelector(
  selectById,
  (event) => {
    if (!event || !event.payments || !event.payments.preference) return null;
    return event.payments.preference.init_point;
  },
);

export const isHighlighted = createSelector(
  selectById,
  event => ((event && event.features) ? event.features.highlight : false),
);
