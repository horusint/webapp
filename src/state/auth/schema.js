/* eslint-disable import/prefer-default-export */
import { schema } from 'normalizr';

import user from 'state/users/schema';

export const relationships = new schema.Entity('relationships', {
  user,
  relatesTo: user,
});
