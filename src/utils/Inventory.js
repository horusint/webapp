const catalog = window.providedCatalog
  ? window.providedCatalog.map(piece => ({
    ...piece,
    content: piece.content.replace('<scrpt', '<script').replace('</scrpt', '</script'),
  }))
  : [];

const inuse = new Set();

function inRange(startTime, endTime) {
  const now = (new Date()).getTime();
  const start = new Date();
  const end = new Date();

  const startTimeArr = startTime.split(':');
  start.setHours(startTimeArr[0], startTimeArr[1], 0);

  const endTimeArr = endTime.split(':');
  end.setHours(endTimeArr[0], endTimeArr[1], 0);

  if (((startTimeArr[0] * 60) + startTimeArr[1]) > ((endTimeArr[0] * 60) + endTimeArr[1])) {
    if (now < start.getTime()) start.setDate(start.getDate() - 1);
    else end.setDate(end.getDate() + 1);
  }

  return (now >= start.getTime() && now <= end.getTime());
}

export default {
  get(zone, device, userType, niches) {
    const date = new Date();
    const now = date.getTime();
    const bag = [];

    catalog.forEach((item) => {
      const startDate = item.startDate ? (new Date(item.startDate)).getTime() : 0;
      const endDate = item.endDate ? (new Date(item.endDate)).getTime() : Infinity;
      const days = item.days || [0, 1, 2, 3, 4, 5, 6];
      const optNiches = item.niches || ['Lesb', 'Gay', 'Maledom', 'Femdom'];
      const devices = item.devices || ['D', 'M'];
      const excludedUsertypes = item.excludedUsertypes || [];
      const userTypes = item.userTypes || ['Unlogg', 'UnConf', 'BasicR', 'Lurker'];

      if (
        (!inuse.has(item.id) || item.canReuse)
        && item.zones.includes(zone)
        && devices.includes(device)
        && userTypes.includes(userType)
        && !excludedUsertypes.includes(userType)
        && optNiches.some(niche => niches.includes(niche))
        && now >= startDate
        && now < endDate
        && (!item.timeRange || inRange(item.timeRange[0], item.timeRange[1]))
        && days.includes(date.getDay())
      ) {
        for (let i = 0; i < item.weight; i += 1) {
          bag.push(item);
        }
      }
    });

    const winner = bag[Math.floor(Math.random() * bag.length)];
    if (!winner) return null;

    inuse.add(winner.id);

    return winner;
  },

  clear() {
    inuse.clear();
  },
};
