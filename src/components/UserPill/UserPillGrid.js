import styled from 'styled-components';

const UserPillGrid = styled.ul`
  display: grid;
  grid-template-columns: 1fr;
  grid-column-gap: 40px;
  grid-row-gap: 24px;
  overflow: auto;
  height: 100%;

  @media(min-width: 768px) {
    grid-template-columns: repeat(2, minmax(0, 1fr));
  }
  @media(min-width: 992px) {
    grid-template-columns: repeat(3, minmax(0, 1fr));
  }
`;

export default UserPillGrid;
