import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import colors from 'utils/css/colors';
import UserType from 'state/users/type';

import Avatar from 'components/UserAvatar';

const Item = styled.li`
  height: 72px;
  padding: 12px;
  border-radius: 72px;
  border: 2px solid #F5F0F0;
  box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.04), 0px 4px 8px rgba(0, 0, 0, 0.08);
  box-sizing: border-box;
  display: flex;
  align-items: center;
  transition: all 250ms ease-out;
  cursor: pointer;
  &:hover {
    border: 2px solid ${colors.red} !important;
  }
`;

const ItemActive = styled(Item)`
  border: 2px solid ${colors.red} !important;
  cursor: default;
`;

const Name = styled.span`
  font-size: 18px;
  margin: 0 0 0 12px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;
`;

const UserPill = ({
  onClick,
  selected,
  user,
}) => {
  const ItemComponent = selected ? ItemActive : Item;
  return (
    <ItemComponent
      onClick={onClick}
      selected={selected}
    >
      <Avatar userId={user.id} />
      <Name>{user.displayname}</Name>
    </ItemComponent>
  );
};

UserPill.propTypes = {
  user: UserType.isRequired,
  selected: PropTypes.bool,
  onClick: PropTypes.func.isRequired,
};

UserPill.defaultProps = {
  selected: false,
};

export default UserPill;
