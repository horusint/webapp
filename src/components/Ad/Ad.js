import React, {
  useEffect, useLayoutEffect, useCallback, useRef, useState,
} from 'react';
import PropTypes from 'prop-types';
import Cookies from 'js-cookie';
import shortid from 'shortid';
import postscribe from 'postscribe';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import isMobile from 'utils/isMobile';
import Inventory from 'utils/Inventory';
import * as authSelectors from 'state/auth/selectors';
import * as appActions from 'state/app/actions';

import Information from 'components/Icons/Information';
import BoxDivider from 'components/BoxDivider';

import BoxWrapper from './BoxWrapper';
import Info from './Info';
import { GUEST_MAX_THREADS } from '../../constants';

const Ad = ({ id, divider }) => {
  const dispatch = useDispatch();
  const elementRef = useRef(null);
  const done = useRef(false);
  const processed = useRef(false);
  const zoneName = useRef(null);
  const postcribeHandler = useRef(null);

  const [piece, setPiece] = useState(null);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn);
  const userHasEmailConfirmed = useSelector(authSelectors.selectIsEmailConfirmed);
  const niches = useSelector(authSelectors.selectNiches, shallowEqual);

  useLayoutEffect(() => {
    if (!done.current) {
      zoneName.current = `${id.replace(/(\s)/g, '').toLowerCase()}-${shortid.generate()}`;
      done.current = true;
      const device = isMobile ? 'M' : 'D';

      let usertype = 'Unlogg';
      if (userIsLoggedIn) usertype = 'UnConf';
      if (userHasEmailConfirmed) usertype = 'BasicR';
      if (
        !userIsLoggedIn
        && parseInt(Cookies.get('threadsRead'), 10) >= GUEST_MAX_THREADS
        && !/bot|crawler|spider|crawling/i.test(navigator.userAgent)
      ) usertype = 'Lurker';

      const result = Inventory.get(id, device, usertype, niches);
      if (result) setPiece(result);
    }
  }, [id, userIsLoggedIn, userHasEmailConfirmed, niches]);

  useEffect(() => {
    const handler = postcribeHandler.current;

    if (elementRef.current && !processed.current) {
      processed.current = true;
      postcribeHandler.current = postscribe(`#${zoneName.current}-content`, piece.content);
    }

    return () => {
      if (postcribeHandler.current && postcribeHandler.current.cancel === 'function') {
        handler.cancel();
      }
    };
  }, [piece, id]);

  const onInfoClick = useCallback(() => {
    dispatch(appActions.openAdInformation());
  }, [dispatch]);

  if (!piece) return null;

  return (
    <BoxWrapper
      topDivider={divider === 'top'}
      ref={elementRef}
      size={piece.size}
    >
      {divider === 'top' && <BoxDivider top />}

      <div id={`${zoneName.current}-content`} className="ad-content" />

      <Info onClick={onInfoClick}>
        Ad
        <Information color="#666" />
      </Info>

      {divider === 'bottom' && <BoxDivider />}
    </BoxWrapper>
  );
};

Ad.propTypes = {
  id: PropTypes.string.isRequired,
  divider: PropTypes.oneOf(['bottom', 'top']),
  data: PropTypes.shape({
    index: PropTypes.number,
  }),
};

Ad.defaultProps = {
  divider: 'bottom',
  data: {},
};

const isSameAd = (prevState, nextState) => {
  if (prevState.id !== nextState.id) return false;
  if (prevState.data && nextState.data && prevState.data.index !== nextState.data.index) {
    return false;
  }
  return true;
};

export default React.memo(Ad, isSameAd);
