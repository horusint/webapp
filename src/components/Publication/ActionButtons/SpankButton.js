import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as feedSelectors from 'state/feed/selectors';

import colors from 'utils/css/colors';

import { Spank } from 'components/Icons';

const SpankButton = ({ publicationId, outline }) => {
  const spankCount = useSelector(
    state => feedSelectors.publications.selectSpankCount(state, publicationId),
  );

  return (
    <span>
      <Spank color={colors.red} outline={outline} />
      {spankCount > 0 && (
        <span>{spankCount}</span>
      )}
    </span>
  );
};

SpankButton.propTypes = {
  publicationId: PropTypes.string.isRequired,
  outline: PropTypes.bool.isRequired,
};

SpankButton.defaultProps = {
};

export default SpankButton;
