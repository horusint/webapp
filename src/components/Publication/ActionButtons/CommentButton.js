import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import colors from 'utils/css/colors';
import * as feedSelectors from 'state/feed/selectors';

import { Comment } from 'components/Icons';

const CommentButton = ({ publicationId, outline }) => {
  const commentCount = useSelector(
    state => feedSelectors.publications.selectCommentCount(state, publicationId),
  );

  return (
    <span>
      <Comment color={colors.red} outline={outline} />
      {commentCount > 0 && <span>{commentCount}</span>}
    </span>
  );
};

CommentButton.propTypes = {
  publicationId: PropTypes.string.isRequired,
  outline: PropTypes.bool.isRequired,
};

CommentButton.defaultProps = {
};

export default CommentButton;
