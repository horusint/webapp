import React from 'react';
import PropTypes from 'prop-types';

import { Container, Actions, CommentContainer } from './UI';
import Header from './Header';
import MoreContent from './MoreContent';
import Content from './Content';
import AddComment from './AddComment';
import Comments from './Comments';
import Share from './ActionButtons/Share';
import Votes from './ActionButtons/Votes';
import Spank from './ActionButtons/Spank';
import Comment from './ActionButtons/Comment';

const Publication = React.memo(({ publicationId, full }) => (
  <Container>
    <Header publicationId={publicationId} />

    <MoreContent publicationId={publicationId} />
    <Content publicationId={publicationId} full={full} />

    <Actions>
      <Spank publicationId={publicationId} />
      <Comment publicationId={publicationId} />
      <Votes publicationId={publicationId} />
      <Share publicationId={publicationId} />
    </Actions>
    <CommentContainer>
      <Comments publicationId={publicationId} full={full} />
      <AddComment publicationId={publicationId} />
    </CommentContainer>
  </Container>
));

Publication.propTypes = {
  publicationId: PropTypes.string.isRequired,
  full: PropTypes.bool,
};

Publication.defaultProps = {
  full: false,
};

export default Publication;
