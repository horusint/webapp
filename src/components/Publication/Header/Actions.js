import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';

import ContextMenu from 'components/ContextMenu';
import SpankListModal from 'components/SpankListModal';

import Follow from '../Follow';
import CloseComments from '../CloseComments';
import EditModal from '../Modals/Edit';
import RemoveModal from '../Modals/Remove';
import VotesModal from '../Modals/Votes';
import locales from '../i18n';

const Actions = ({ publicationId }) => {
  const { t } = useTranslation(locales);

  const userIsAuthor = useSelector(
    state => feedSelectors.publications.userIsAuthor(state, publicationId),
  );
  const isPoll = useSelector(
    state => feedSelectors.publications.isPoll(state, publicationId),
  );

  const [showingSpanksModal, setShowingSpanksModal] = useState(false);
  const showSpanksModal = () => setShowingSpanksModal(true);
  const hideSpanksModal = () => setShowingSpanksModal(false);

  const [showingEditModal, setShowingEditModal] = useState(false);
  const showEditModal = () => setShowingEditModal(true);
  const hideEditModal = () => setShowingEditModal(false);

  const [showingRemoveModal, setShowingRemoveModal] = useState(false);
  const showRemoveModal = () => setShowingRemoveModal(true);
  const hideRemoveModal = () => setShowingRemoveModal(false);

  const [showingVotesModal, setShowingVotesModal] = useState(false);
  const showVotesModal = () => setShowingVotesModal(true);
  const hideVotesModal = () => setShowingVotesModal(false);

  const contextItems = [
    { key: 'showspanks', onClick: showSpanksModal, component: t('Show spanks') },
    { key: 'follow', component: <Follow publicationId={publicationId} /> },
  ];
  if (userIsAuthor) {
    contextItems.push({
      key: 'closecomments', component: <CloseComments publicationId={publicationId} />,
    });

    if (isPoll) {
      contextItems.push({
        key: 'votes',
        onClick: showVotesModal,
        component: t('Votes'),
      });
    }

    contextItems.push({
      key: 'edit',
      onClick: showEditModal,
      component: t('global:Edit'),
    });
    contextItems.push({
      key: 'remove',
      onClick: showRemoveModal,
      component: t('Remove'),
      danger: true,
    });
  }

  return (
    <>
      <ContextMenu
        items={contextItems}
        size={5}
      />

      {/* Modals */}
      {showingSpanksModal && (
        <SpankListModal type="publication" entityId={publicationId} close={hideSpanksModal} />
      )}
      {showingEditModal && (
        <EditModal publicationId={publicationId} close={hideEditModal} />
      )}
      {showingRemoveModal && (
        <RemoveModal publicationId={publicationId} close={hideRemoveModal} />
      )}
      {showingVotesModal && (
        <VotesModal publicationId={publicationId} close={hideVotesModal} />
      )}
    </>
  );
};

Actions.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Actions.defaultProps = {
};

export default Actions;
