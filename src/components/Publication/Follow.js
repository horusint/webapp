import React, { useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as feedSelectors from 'state/feed/selectors';
import * as feedActions from 'state/feed/actions';
import * as appActions from 'state/app/actions';

import Toggle from 'components/Toggle';

import locales from './i18n';

const Follow = ({ publicationId }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const [toggling, setToggling] = useState(false);
  const following = useSelector(
    state => feedSelectors.publications.following(state, publicationId),
  );

  const toggle = useCallback(async () => {
    try {
      setToggling(true);
      await dispatch(feedActions.toggleFollowPublication(publicationId));
      dispatch(appActions.addToast(t('Subscription updated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setToggling(false);
  }, [publicationId, dispatch, t]);

  return (
    <Toggle label={t('Follow publication')} active={following} onChange={toggle} loading={toggling} />
  );
};

Follow.propTypes = {
  publicationId: PropTypes.string.isRequired,
};

Follow.defaultProps = {
};

export default Follow;
