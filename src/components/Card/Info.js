import styled from 'styled-components';

import colors from 'utils/css/colors';

const Info = styled.div`
  font-size: 12px;
  color: ${colors.grey};
  margin: -12px 0 12px;
`;

export default Info;
