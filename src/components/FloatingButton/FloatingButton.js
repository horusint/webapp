import React from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { Fab, Action } from 'react-tiny-fab';
import 'react-tiny-fab/dist/styles.css';

import colors from 'utils/css/colors';
import isMobile from 'utils/isMobile';

import { Plus } from 'components/Icons';

const style = { width: '32px' };
const position = isMobile
  ? { right: 0 }
  : { marginLeft: 610 };

const FloatingButton = ({ actions }) => {
  const history = useHistory();
  return (
    <Fab
      mainButtonStyles={{
        backgroundColor: colors.red,
      }}
      position={{
        bottom: 0,
        position: 'fixed',
        zIndex: 1,
        ...position,
      }}
      icon={<Plus color="white" style={style} />}
      event="click"
    >
      {actions.map((action) => {
        const onClick = action.onClick || (() => history.push(action.to));
        return (
          <Action
            key={`floating-action-button-${action.text}`}
            text={action.text}
            onClick={onClick}
            style={{ backgroundColor: colors.mainDark }}
          >
            {action.icon}
          </Action>
        );
      })}
    </Fab>
  );
};

FloatingButton.propTypes = {
  actions: PropTypes.arrayOf(PropTypes.shape({
    text: PropTypes.string,
    onClick: PropTypes.func,
    to: PropTypes.string,
    icon: PropTypes.node,
  })).isRequired,
};

FloatingButton.defaultProps = {
};

export default React.memo(FloatingButton);
