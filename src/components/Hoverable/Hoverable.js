/* eslint-disable react-hooks/exhaustive-deps */

import React, { useRef, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import fastdom from 'fastdom';

import colors from 'utils/css/colors';
import isMobile from 'utils/isMobile';

const Tooltip = styled.div`
  color: ${colors.red};
  text-transform: uppercase;
  font-size: 10px;
  position: absolute;
  top: -16px;
  visibility: hidden;
  white-space: nowrap;
`;

const HoverableContainer = styled.div`
  position: relative;
  display: flex;

  &:hover {
    ${Tooltip} {
      visibility: visible;
    }
  }
`;

const Hoverable = ({ normal, hover, tooltip }) => {
  const el = useRef(null);
  const elTooltip = useRef(null);
  const [isHovered, setIsHovered] = useState(false);

  const onMouseEnter = () => {
    if (!isMobile) setIsHovered(true);
  };
  const onMouseLeave = () => {
    if (!isMobile) setIsHovered(false);
  };

  useEffect(() => {
    const element = el.current;

    if (element) {
      el.current.addEventListener('mouseenter', onMouseEnter);
      el.current.addEventListener('mouseleave', onMouseLeave);
    }

    return () => {
      element.removeEventListener('mouseenter', onMouseEnter);
      element.removeEventListener('mouseleave', onMouseLeave);
    };
  }, []);

  useEffect(() => {
    if (tooltip) {
      fastdom.measure(() => {
        if (elTooltip.current && el.current) {
          const tw = elTooltip.current.offsetWidth;
          const ew = el.current.offsetWidth;

          fastdom.mutate(() => {
            if (elTooltip.current) {
              elTooltip.current.style.left = `${(-tw / 2) + (ew / 2)}px`;
            }
          });
        }
      });
    }
  }, [tooltip]);

  return (
    <HoverableContainer ref={el}>
      {tooltip && <Tooltip ref={elTooltip}>{tooltip}</Tooltip>}
      {isHovered ? hover : normal}
    </HoverableContainer>
  );
};

Hoverable.propTypes = {
  normal: PropTypes.node.isRequired,
  hover: PropTypes.node.isRequired,
  tooltip: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.string,
  ]),
};

Hoverable.defaultProps = {
  tooltip: null,
};

export default Hoverable;
