/* eslint-disable no-param-reassign */
import visit from 'unist-util-visit';

const EMOJI_RE = /:\+1:|:-1:|:[\w-]+:(:skin-tone-(\d):)?/;
const HASHTAG_RE = /(^|\s)#([a-zA-Z0-9\u00C0-\u00ff][\u00C0-\u00ff\w-]*){2,}/;
const MENTION_RE = /(^|\s)(@([a-zA-Z0-9_]){3,})/;

const ALL_RE = new RegExp([EMOJI_RE.source, HASHTAG_RE.source, MENTION_RE.source].join('|'), 'gi');

const getType = (value) => {
  if (EMOJI_RE.test(value)) return 'emoji';
  if (HASHTAG_RE.test(value)) return 'hashtag';
  if (MENTION_RE.test(value)) return 'mention';

  return 'undefined';
};

const extractText = (string, start, end) => {
  const startLine = string.slice(0, start).split('\n');
  const endLine = string.slice(0, end).split('\n');

  return {
    type: 'text',
    value: string.slice(start, end),
    position: {
      start: {
        line: startLine.length,
        column: startLine[startLine.length - 1].length + 1,
      },
      end: {
        line: endLine.length,
        column: endLine[endLine.length - 1].length + 1,
      },
    },
  };
};

const plugin = () => {
  function transformer(tree) {
    visit(tree, 'text', (node, position, parent) => {
      const definition = [];
      let lastIndex = 0;
      let match;

      // eslint-disable-next-line no-cond-assign
      while ((match = ALL_RE.exec(node.value)) !== null) {
        const value = match[0];
        const type = getType(value);

        if (match.index !== lastIndex) {
          definition.push(extractText(node.value, lastIndex, match.index));
        }

        definition.push({
          type,
          value,
        });

        lastIndex = match.index + value.length;
      }

      if (lastIndex !== node.value.length) {
        const text = extractText(node.value, lastIndex, node.value.length);
        definition.push(text);
      }

      const last = parent.children.slice(position + 1);
      parent.children = parent.children.slice(0, position);
      parent.children = parent.children.concat(definition);
      parent.children = parent.children.concat(last);
    });
  }

  return transformer;
};

export default plugin;
