import React, { useMemo } from 'react';
import PropTypes from 'prop-types';
import ReactMarkdown from 'react-markdown';

import mfmparser from './mfmparser';
import Wrapper from './Wrapper';
import Emoji from './Emoji';
import Hashtag from './Hashtag';
import Mention from './Mention';
import Link from './Link';
import Image from './Image';

const types = ['emphasis', 'strong', 'blockquote', 'delete', 'link', 'image', 'linkReference', 'imageReference', 'table', 'tableHead', 'tableBody', 'tableRow', 'tableCell', 'list', 'listItem', 'definition', 'heading', 'inlineCode', 'code'];

const isEmojiOnly = content => content.replace(/^(:([a-zA-Z0-9+-][\w-]*):)(:skin-tone-(\d):)?/, '').trim() === '';

const ParsedContent = ({
  content, emojis, hashtags, mentions, markdown, emojiSize, disallowed, emojiOnlySize,
}) => {
  const eSize = emojiOnlySize && isEmojiOnly(content) ? emojiOnlySize : emojiSize;

  const renderers = {
    link: Link,
    image: Image,
  };
  if (emojis) {
    const EmojiComponent = ({ value }) => <Emoji value={value} size={eSize} />;
    EmojiComponent.propTypes = { value: PropTypes.string.isRequired };
    renderers.emoji = EmojiComponent;
  }
  if (hashtags) renderers.hashtag = Hashtag;
  if (mentions) renderers.mention = Mention;

  const disallowedTypes = useMemo(() => (markdown ? disallowed : types), [markdown, disallowed]);

  return (
    <Wrapper emojiSize={eSize}>
      <ReactMarkdown
        source={(content || '').replace(/\n/gi, '  \n')}
        plugins={[
          mfmparser,
        ]}
        renderers={renderers}
        disallowedTypes={disallowedTypes}
        unwrapDisallowed
      />
    </Wrapper>
  );
};

ParsedContent.propTypes = {
  content: PropTypes.string.isRequired,
  emojis: PropTypes.bool,
  mentions: PropTypes.bool,
  hashtags: PropTypes.bool,
  markdown: PropTypes.bool,
  emojiSize: PropTypes.number,
  disallowed: PropTypes.arrayOf(PropTypes.oneOf(types)),
  emojiOnlySize: PropTypes.number,
};

ParsedContent.defaultProps = {
  emojis: true,
  mentions: true,
  hashtags: true,
  markdown: true,
  emojiSize: 20,
  disallowed: [],
  emojiOnlySize: null,
};

export default ParsedContent;
