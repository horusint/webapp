import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as channelSelectors from 'state/channels/selectors';
import * as appActions from 'state/app/actions';
import * as channelsActions from 'state/channels/actions';

import { useTranslation } from 'hooks';
import ChannelType from 'state/channels/type';

import Button from 'components/Button';

import locales from './i18n';

const ChannelMembershipRequestButton = ({ channel, full, style }) => {
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation(locales);

  const isUserLoggedIn = useSelector(authSelectors.loggedIn);
  const isUserSubscribed = useSelector(state => channelSelectors.amISubscribed(state, channel.id));

  const [joining, setJoining] = useState(false);

  if (isUserSubscribed) {
    return <Button to={`/chat/channels/${channel.id}`} color="white" fontColor="#666" full={full} style={style}>{t('Already a member')}</Button>;
  }

  const join = async () => {
    if (isUserLoggedIn) {
      try {
        setJoining(true);
        await dispatch(channelsActions.join(channel.id));
        dispatch(appActions.addToast(t('You have joined the channel')));
        history.push(`/chat/channels/${channel.id}`);
      } catch (error) {
        dispatch(appActions.addError(error));
        setJoining(false);
      }
    } else {
      history.push(`/chat/channels/${channel.id}`);
    }
  };

  return <Button onClick={join} loading={joining} full={full} style={style}>{t('Join')}</Button>;
};

ChannelMembershipRequestButton.propTypes = {
  channel: ChannelType.isRequired,
  full: PropTypes.bool,
  // eslint-disable-next-line react/forbid-prop-types
  style: PropTypes.object,
};

ChannelMembershipRequestButton.defaultProps = {
  full: true,
  style: {},
};

export default ChannelMembershipRequestButton;
