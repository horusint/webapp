import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import Menu, { Item } from 'components/Menu';

const ItemContainer = styled.div`
  svg {
    width: 16px !important;
    margin-right: 16px !important;

    path {
      fill: #666;
    }
  }
`;

const OptionsCaret = ({ options, onChange, close }) => {
  const onItemSelect = value => (e) => {
    e.stopPropagation();
    e.preventDefault();
    onChange(value);
    close();
  };

  return (
    <Menu open onClose={close}>
      {options.map(option => (
        <Item key={option.value} onClick={onItemSelect(option.value)}>
          <ItemContainer>
            {option.icon}
            <span>{option.label}</span>
          </ItemContainer>
        </Item>
      ))}
    </Menu>
  );
};

OptionsCaret.propTypes = {
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
      icon: PropTypes.node,
      selected: PropTypes.bool,
    }),
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  close: PropTypes.func.isRequired,
};

OptionsCaret.defaultProps = {
};

export default OptionsCaret;
