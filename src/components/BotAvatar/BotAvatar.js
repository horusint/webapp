import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import * as appSelectors from 'state/app/selectors';

import defaultAvatar from './default.png';

const AvatarWrapper = styled.div.attrs({
  className: 'avatar',
})`
  position: relative;
  width: ${props => props.size};
  height: ${props => props.size};

  ${props => props.onClick && `
    cursor: pointer;
  `}
`;

const AvatarImg = styled.img.attrs(({ bot, isSafeForWork }) => ({
  src: (!bot || bot.loading || isSafeForWork || !bot.avatar) ? defaultAvatar : bot.avatar,
}))`
  border-radius: 100%;
  width: ${props => props.size};
  height: ${props => props.size};
  box-shadow: 0px 2px 7px rgba(0, 0, 0, 0.12);
  flex-shrink: 0;
  vertical-align: middle;
`;

const botEqual = (prevBot, nextBot) => (
  prevBot.id === nextBot.id
  && prevBot.avatar === nextBot.avatar
  && prevBot.loading === nextBot.loading
);

const BotAvatar = ({ size, botId, ...props }) => {
  const bot = useSelector(state => channelSelectors.getBotById(state, botId), botEqual);
  const isSafeForWork = useSelector(appSelectors.selectIsSafeForWork);

  return (
    <AvatarWrapper size={size} {...props}>
      <AvatarImg bot={bot} size={size} isSafeForWork={isSafeForWork} />
    </AvatarWrapper>
  );
};

BotAvatar.propTypes = {
  botId: PropTypes.string.isRequired,
  size: PropTypes.string,
};

BotAvatar.defaultProps = {
  size: '50px',
};

const equality = (prevProps, nextProps) => (
  prevProps.userId === nextProps.userId
  && prevProps.size === nextProps.size
);
export default React.memo(BotAvatar, equality);
