import React from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as replySelectors from 'state/replies/selectors';
import * as threadSelectors from 'state/threads/selectors';
import * as feedSelectors from 'state/feed/selectors';

import Modal from 'components/Modal';
import { SelectableList } from 'components/SelectableList';

import User from './User';
import locales from './i18n';

const getSelector = (type) => {
  switch (type) {
    case 'publication':
      return feedSelectors.publications;
    case 'comment':
      return feedSelectors.comments;
    case 'reply':
      return replySelectors;
    case 'thread':
    default:
      return threadSelectors;
  }
};

const SpankListModal = ({ close, entityId, type }) => {
  const { t } = useTranslation(locales);

  const selector = getSelector(type);
  const spanks = useSelector(state => selector.selectReactionsList(state, entityId), shallowEqual);

  return (
    <Modal
      title={t('global:Spanks')}
      onClose={close}
    >
      <SelectableList>
        {spanks.reverse().map(userId => (
          <User key={`spank-list-${userId}`} userId={userId} />
        ))}
      </SelectableList>
    </Modal>
  );
};

SpankListModal.propTypes = {
  close: PropTypes.func.isRequired,
  entityId: PropTypes.string.isRequired,
  type: PropTypes.oneOf(['thread', 'reply', 'publication', 'comment']).isRequired,
};

export default withRouter(SpankListModal);
