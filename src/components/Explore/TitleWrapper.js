import styled from 'styled-components';

const TitleWrapper = styled.div`
  text-align: center;
  margin-bottom: 24px;
`;

export default TitleWrapper;
