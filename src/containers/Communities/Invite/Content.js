import React from 'react';
import PropTypes from 'prop-types';

import CommunityInvite from 'components/CommunityInvite';

const Content = ({ communityId }) => (
  <CommunityInvite communityId={communityId} />
);

Content.propTypes = {
  communityId: PropTypes.string.isRequired,
};

Content.defaultProps = {
};

export default Content;
