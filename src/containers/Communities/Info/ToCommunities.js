import React from 'react';
import { Link } from 'react-router-dom';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';

import { ArrowBack } from 'components/Icons';
import LeftColumnBottom from 'components/LeftColumnBottom';

import locales from './i18n';

const ToCommunities = () => {
  const { t } = useTranslation(locales);

  return (
    <LeftColumnBottom>
      <Link to="/communities">
        <ArrowBack color={colors.redReactions} />
        <span>{t('Back to communities')}</span>
      </Link>
    </LeftColumnBottom>
  );
};

ToCommunities.propTypes = {
};

ToCommunities.defaultProps = {
};

export default ToCommunities;
