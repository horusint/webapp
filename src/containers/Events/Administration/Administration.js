import React from 'react';

import { FlexWrapper, FlexInnerWrapper, FlexContainer } from 'components/FlexWrapper';
import Layout from 'components/Layout';
import Sidebar from 'containers/Sidebar';

import Header from './Header';
import Content from './Content';

const Administration = () => (
  <Layout columns={2} feed leftColumnOpen={false} rightColumnOpen={false}>
    <FlexWrapper canOverflow>
      <FlexInnerWrapper>
        <Header />

        <FlexContainer framed>
          <Content />
        </FlexContainer>
      </FlexInnerWrapper>
    </FlexWrapper>

    <Sidebar />
  </Layout>
);

Administration.propTypes = {
};

Administration.defaultProps = {
};

export default Administration;
