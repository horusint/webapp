import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';
import * as eventActions from 'state/events/actions';
import * as appActions from 'state/app/actions';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import locales from '../i18n';

const Wrapper = styled.div`
  text-align: center;

  p {
    margin-bottom: 32px;
  }

  .qr {

  }
`;
Wrapper.displayName = 'Wrapper';

const QR = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const { eventId } = useParams();

  const hasAccess = useSelector(
    state => eventSelectors.hasAssistanceFeatureEnabled(state, eventId),
  );
  const qr = useSelector(state => eventSelectors.selectQR(state, eventId));

  const [regenerating, setRegenerating] = useState(false);

  const regenerate = useCallback(async () => {
    try {
      setRegenerating(true);

      await dispatch(eventActions.regenerateQR(eventId));

      dispatch(appActions.addToast(t('QR regenerated')));
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setRegenerating(false);
  }, [dispatch, t, eventId]);

  if (!hasAccess) return <EmptyState title={t('This Event doesn\'t have assistance tracking enabled')} />;

  return (
    <Wrapper>
      <p>{t('You can generate a QR code for users, so they can scan and mark themeselves as assisting to your Event.')}</p>

      {qr && (
        <div className="qr">
          <img src={qr} alt="QR" />
        </div>
      )}

      <Button color="white" fontColor="black" onClick={regenerate} loading={regenerating}>
        {t(qr ? 'Regenerate' : 'Generate')}
      </Button>
    </Wrapper>
  );
};

QR.propTypes = {
};

QR.defaultProps = {
};

export default QR;
