import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as userSelectors from 'state/users/selectors';

import UserLink from 'components/UserLink';
import UserDisplayName from 'components/UserDisplayName';
import UserAvatar from 'components/UserAvatar';

const User = ({ userId }) => {
  const username = useSelector(userSelectors.getUsername(userId));

  return (
    <UserLink userId={userId}>
      <div className="userrow">
        <UserAvatar userId={userId} size="32px" />
        <div>
          <UserDisplayName userId={userId} />
          <div className="username">{`@${username}`}</div>
        </div>
      </div>
    </UserLink>
  );
};

User.propTypes = {
  userId: PropTypes.number.isRequired,
};

User.defaultProps = {
};

export default User;
