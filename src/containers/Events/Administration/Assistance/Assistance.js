import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as eventSelectors from 'state/events/selectors';

import Filter from 'components/Filter';

import Wrapper from './Wrapper';
import Rsvp from './Rsvp';
import locales from '../i18n';

const Assistance = () => {
  const { t } = useTranslation(locales);
  const { eventId } = useParams();

  const rsvps = useSelector(state => eventSelectors.selectRsvps(state, eventId), shallowEqual);

  const [filter, setFilter] = useState('');

  return (
    <Wrapper>
      <div className="counter">
        <span>{t('Assistants {{count}}', { count: rsvps.filter(r => !!r.assistedAt).length })}</span>
        <Filter value={filter} onChange={e => setFilter(e.target.value)} placeholder={t('global:Filter')} />
      </div>

      <table>
        <thead>
          <tr>
            <th>{t('User')}</th>
            <th>{t('Action')}</th>
          </tr>
        </thead>
        <tbody>
          {rsvps.map(rsvp => (
            <Rsvp key={`assistance-${rsvp.id}`} rsvp={rsvp} filter={filter} />
          ))}
        </tbody>
      </table>
    </Wrapper>
  );
};

Assistance.propTypes = {
};

Assistance.defaultProps = {
};

export default Assistance;
