import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import { useTranslation } from 'hooks';

import Toggle from 'components/Toggle';

import Wrapper from './Wrapper';
import locales from '../i18n';

const Features = ({
  price, assistance, setAssistance, highlight, setHighlight,
  transactional, setTransactional,
}) => {
  const { t } = useTranslation(locales);

  const assistanceChanged = useCallback((v) => {
    setAssistance(v.target.checked);
  }, [setAssistance]);

  const highlightChanged = useCallback((v) => {
    setHighlight(v.target.checked);
  }, [setHighlight]);

  const transactionalChanged = useCallback((v) => {
    setTransactional(v.target.checked);
  }, [setTransactional]);

  return (
    <Wrapper>
      <h3>{t('Premium options')}</h3>

      <div>
        <Toggle position="left" label={t('Highlight')} active={highlight} onChange={highlightChanged} />
        <p>{t('highlight.details')}</p>
        <p><strong>{t('highlight.pricing', { highlightPrice: 100 })}</strong></p>
      </div>

      <div>
        <Toggle position="left" label={t('Assistance tracking')} active={assistance} onChange={assistanceChanged} />
        <p>{t('assistance.details')}</p>
        <p><strong>{t('assistance.pricing', { assistanceCommission: (price * 0.1).toFixed(2) })}</strong></p>
      </div>

      <div>
        <Toggle position="left" label={t('Transactional e-mails')} active={transactional} onChange={transactionalChanged} />
        <p>{t('transactional.details')}</p>
        <p><strong>{t('transactional.pricing')}</strong></p>
      </div>

      <div>
        <Toggle position="left" label={t('Early tickets')} disabled active={false} />
        <p>{t('early.details')}</p>
        <p><strong>{t('early.pricing', { assistanceCommission: (price * 0.1).toFixed(2) })}</strong></p>
      </div>
    </Wrapper>
  );
};

Features.propTypes = {
  price: PropTypes.number.isRequired,
  assistance: PropTypes.bool.isRequired,
  setAssistance: PropTypes.func.isRequired,
  highlight: PropTypes.bool.isRequired,
  setHighlight: PropTypes.func.isRequired,
  transactional: PropTypes.bool.isRequired,
  setTransactional: PropTypes.func.isRequired,
};

Features.defaultProps = {
};

export default Features;
