import styled from 'styled-components';

const Container = styled.div`
  background-color: #323232;
  color: white;
  margin: 0;
  padding: 50px;
  display: flex;

  @media(max-width: 767px) {
    padding: 30px 20px;
  }
`;

export default Container;
