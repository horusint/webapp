import React, { useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import { useInputValue, useTranslation, useFocusOnMount } from 'hooks';

import AuthLayout from 'components/AuthLayout';
import Input from 'components/Forms/InputSimple';
import Button from 'components/Button';

import locales from './i18n';

const ResetPassword = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const history = useHistory();
  const params = useParams();

  const passwordEl = useFocusOnMount();
  const [reseting, setReseting] = useState(false);
  const password = useInputValue('');
  const passwordconfirm = useInputValue('');
  const [passwordConfirmError, setPasswordConfirmError] = useState(null);

  const reset = async () => {
    try {
      setReseting(true);
      setPasswordConfirmError(null);

      const hasError = passwordconfirm.value.length && passwordconfirm.value !== password.value;
      if (!hasError) {
        await dispatch(authActions.reset(params.hash, password.value));
        dispatch(appActions.addToast(t('Password reseted successfuly')));
        history.push('/');
      } else {
        setPasswordConfirmError(t('Passwords doesn\'t match'));
        setReseting(false);
      }
    } catch (err) {
      let error = err.message;
      if (err.response) {
        // eslint-disable-next-line
        error = err.response.data.error;
      } else if (err.request) {
        error = err.request;
      }

      dispatch(appActions.addError(error));
      setReseting(false);
    }
  };

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') reset();
  };

  return (
    <AuthLayout
      title={t('Enter your new password')}
    >
      <Input
        ref={passwordEl}
        placeholder={t('New Password')}
        {...password}
        disabbled={reseting}
        withBorder
        type="password"
      />
      <Input
        placeholder={t('Confirm Password')}
        {...passwordconfirm}
        disabbled={reseting}
        onKeyPress={handleKeyPress}
        withBorder
        error={passwordConfirmError}
        type="password"
      />

      <Button
        onClick={reset}
        loading={reseting}
        content={t('Reset password')}
      />
    </AuthLayout>
  );
};

ResetPassword.propTypes = {
};

export default ResetPassword;
