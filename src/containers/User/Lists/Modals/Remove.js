import React, { useCallback, useState, useMemo } from 'react';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Modal from 'components/Modal';
import Toggle from 'components/Toggle';
import Button from 'components/Button';
import Select from 'components/Select';

import locales from '../i18n';

const Remove = ({ listId, close }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const lists = useSelector(authSelectors.selectUserLists, shallowEqual);

  const [loading, setLoading] = useState(false);
  const [move, setMove] = useState(false);
  const changeMove = useCallback((evt) => {
    setMove(evt.target.checked);
  }, []);

  const options = useMemo(() => (
    lists
      .filter(l => l.id !== listId)
      .map(l => ({ label: l.name, value: l.id }))
  ), [lists, listId]);

  const [moveTo, setMoveTo] = useState(options.length > 0 ? options[0].value : null);
  const changeMoveTo = useCallback((v) => {
    setMoveTo(v.value);
  }, []);

  const remove = useCallback(async () => {
    try {
      setLoading(true);

      await dispatch(authActions.removeFollowList(listId, moveTo));
      dispatch(appActions.addToast(t('List removed')));

      history.push('/user/lists');
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
  }, [dispatch, t, history, listId, moveTo]);

  return (
    <Modal
      title={t('Delete list')}
      onCancel={close}
      actions={[
        <Button key="followlist-delete-confirm" onClick={remove} loading={loading}>{t('global:Confirm')}</Button>,
      ]}
    >
      {t('Are you sure you want to remove this contact list?')}

      {options.length > 0 && (
        <div>
          <br />
          <br />

          <Toggle
            label={t('Move all contacts to existing list')}
            active={move}
            onChange={changeMove}
            position="left"
          />

          <br />

          <Select
            value={options.find(o => o.value === moveTo)}
            options={options}
            onChange={changeMoveTo}
            isDisabled={!move}
          />
        </div>
      )}
    </Modal>
  );
};

Remove.propTypes = {
  listId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

Remove.defaultProps = {
};

export default Remove;
