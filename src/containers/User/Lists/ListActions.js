import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { useTranslation, useOpenClose } from 'hooks';

import Button from 'components/Button';

import RenameModal from './Modals/Rename';
import RemoveModal from './Modals/Remove';
import locales from './i18n';

const Wrapper = styled.div`
  margin-bottom: 32px;
  display: flex;
  justify-content: flex-end;

  button {
    margin-left: 16px;
  }
`;
Wrapper.displayName = 'Wrapper';

const ListActions = ({ listId }) => {
  const { t } = useTranslation(locales);

  const [isOpenRenameModal, openRenameModal, closeRenameModal] = useOpenClose(false);
  const [isOpenRemoveModal, openRemoveModal, closeRemoveModal] = useOpenClose(false);

  return (
    <Wrapper>
      <Button color="white" fontColor="black" onClick={openRenameModal}>{t('Rename')}</Button>
      <Button onClick={openRemoveModal}>{t('Delete')}</Button>

      {/* Modals */}
      {isOpenRenameModal && (
        <RenameModal listId={listId} close={closeRenameModal} />
      )}

      {isOpenRemoveModal && (
        <RemoveModal listId={listId} close={closeRemoveModal} />
      )}
    </Wrapper>
  );
};

ListActions.propTypes = {
  listId: PropTypes.string.isRequired,
};

ListActions.defaultProps = {
};

export default ListActions;
