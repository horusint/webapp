import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, shallowEqual } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import { SelectableList } from 'components/SelectableList';
import EmptyState from 'components/EmptyState';

import locales from './i18n';
import User from './User';

const ListUsers = ({ listId }) => {
  const { t } = useTranslation(locales);

  const users = useSelector(
    state => authSelectors.selectUserListUsers(state, listId),
    shallowEqual,
  );

  return (
    <SelectableList>
      {users.map(userId => (
        <User key={`lists-user-${userId}`} userId={userId} listId={listId} />
      ))}

      {!users.length && (
        <EmptyState title={t('There are no contacts in this list yet')} />
      )}
    </SelectableList>
  );
};

ListUsers.propTypes = {
  listId: PropTypes.string.isRequired,
};

ListUsers.defaultProps = {
};

export default ListUsers;
