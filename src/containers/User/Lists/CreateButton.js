import React, {
  useCallback, useRef, useState, useEffect,
} from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';
import * as authActions from 'state/auth/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import Modal from 'components/Modal';
import Input from 'components/Forms/Input';

import locales from './i18n';

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 0 16px 16px;
`;
Wrapper.displayName = 'Wrapper';

const CreateButton = () => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();
  const history = useHistory();

  const inputEl = useRef(null);
  const [isOpenCreateModal, openCreateModal, closeCreateModal] = useOpenClose(false);

  const [loading, setLoading] = useState(false);
  const name = useInputValue('');

  useEffect(() => {
    if (inputEl.current && isOpenCreateModal) {
      inputEl.current.focus();
    }
  }, [inputEl, isOpenCreateModal]);

  const create = useCallback(async () => {
    try {
      setLoading(true);
      const { id } = await dispatch(authActions.createFollowList(name.value));
      dispatch(appActions.addToast(t('Follow List created')));
      history.push(`/user/lists/${id}`);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
    closeCreateModal();
  }, [dispatch, t, closeCreateModal, history, name.value]);

  return (
    <Wrapper>
      <Button color="white" fontColor="black" onClick={openCreateModal}>{t('Create new one')}</Button>

      {isOpenCreateModal && (
        <Modal
          title={t('Create Follow List')}
          onCancel={closeCreateModal}
          actions={[
            <Button key="add-followlist-confirm" loading={loading} onClick={create}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Input the name for your new Follow List')}

          <br />
          <br />

          <Input {...name} ref={inputEl} placeholder={t('Name')} />
        </Modal>
      )}
    </Wrapper>
  );
};

CreateButton.propTypes = {
};

CreateButton.defaultProps = {
};

export default CreateButton;
