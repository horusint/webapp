import React, {
  useCallback, useRef, useState, useEffect,
} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useDispatch } from 'react-redux';

import { useTranslation, useOpenClose, useInputValue } from 'hooks';
import * as feedActions from 'state/feed/actions';
import * as appActions from 'state/app/actions';

import Button from 'components/Button';
import Modal from 'components/Modal';
import Input from 'components/Forms/Input';

import locales from './i18n';

const Wrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  margin: 0 16px 16px;
`;
Wrapper.displayName = 'Wrapper';

const CreateButton = ({ setCollections }) => {
  const { t } = useTranslation(locales);
  const dispatch = useDispatch();

  const inputEl = useRef(null);
  const [isOpenCreateModal, openCreateModal, closeCreateModal] = useOpenClose(false);

  const [loading, setLoading] = useState(false);
  const hashtag = useInputValue('');

  useEffect(() => {
    if (inputEl.current && isOpenCreateModal) {
      inputEl.current.focus();
    }
  }, [inputEl, isOpenCreateModal]);

  const create = useCallback(async () => {
    try {
      setLoading(true);
      const { data } = await dispatch(feedActions.createCollection(hashtag.value));
      dispatch(appActions.addToast(t('Collection created')));
      setCollections(currentValue => [
        ...currentValue,
        data,
      ]);
    } catch (error) {
      dispatch(appActions.addError(error));
    }

    setLoading(false);
    hashtag.change('');
    closeCreateModal();
  }, [dispatch, t, closeCreateModal, hashtag, setCollections]);

  return (
    <Wrapper>
      <Button color="white" fontColor="black" onClick={openCreateModal}>{t('Create new one')}</Button>

      {isOpenCreateModal && (
        <Modal
          title={t('Create Collection')}
          onCancel={closeCreateModal}
          actions={[
            <Button key="add-collection-confirm" loading={loading} onClick={create}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Enter the hashtag for your new Collection')}

          <br />
          <br />

          <Input {...hashtag} ref={inputEl} placeholder={t('Hashtag')} />
        </Modal>
      )}
    </Wrapper>
  );
};

CreateButton.propTypes = {
  setCollections: PropTypes.func.isRequired,
};

CreateButton.defaultProps = {
};

export default CreateButton;
