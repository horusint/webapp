import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as invoiceSelectors from 'state/invoices/selectors';

import Button from 'components/Button';

import locales from './i18n';

const InvoiceRow = ({ invoiceId }) => {
  const { t } = useTranslation(locales);

  const total = useSelector(state => invoiceSelectors.getTotal(state, invoiceId));
  const isPayed = useSelector(state => invoiceSelectors.isPayed(state, invoiceId));

  return (
    <tr>
      <td>{t('Invoice #{{id}}', { id: invoiceId })}</td>
      <td className="total">
        {total}
        {' '}
        ARS
      </td>
      <td>
        {isPayed
          ? <span>{t('Payed')}</span>
          : <Button to={`/user/invoices/${invoiceId}`}>{t('Pay')}</Button>
        }
      </td>
    </tr>
  );
};

InvoiceRow.propTypes = {
  invoiceId: PropTypes.string.isRequired,
};

InvoiceRow.defaultProps = {
};

export default InvoiceRow;
