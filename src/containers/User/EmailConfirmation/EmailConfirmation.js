import React, { useRef } from 'react';
import { useSelector } from 'react-redux';

import { useTranslation } from 'hooks';
import * as authSelectors from 'state/auth/selectors';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import Confirm from './Confirm';
import locales from './i18n';

const EmailConfirmation = () => {
  const { t } = useTranslation(locales);

  const isLoggedIn = useSelector(authSelectors.loggedIn);
  const isEmailConfirmed = useSelector(authSelectors.selectIsEmailConfirmed);

  // Needed because once confirmed, selector will become true
  const alreadyConfirmed = useRef(isEmailConfirmed);

  if (!isLoggedIn) {
    return (
      <EmptyState
        uiLeftColumn
        full
        title={t('You need to be logged in to confirm your user')}
      >
        <Button to="/login">{t('global:Log in')}</Button>
      </EmptyState>
    );
  }

  if (alreadyConfirmed.current) {
    return (
      <EmptyState
        uiLeftColumn
        full
        title={t('Your user account is already confirmed')}
      >
        <Button to="/">{t('Go to home')}</Button>
      </EmptyState>
    );
  }

  return <Confirm />;
};

EmailConfirmation.propTypes = {
};

EmailConfirmation.defaultProps = {
};

export default EmailConfirmation;
