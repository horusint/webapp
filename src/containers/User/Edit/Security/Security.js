import React from 'react';
import styled from 'styled-components';

import StarMode from './StarMode';
import Password from './Password';
import Email from './Email';
import Suspend from './Suspend';

const Wrapper = styled.div`
  margin: 32px;
`;
Wrapper.displayName = 'Wrapper';

const Section = styled.div`
  margin-bottom: 32px;

  h2 {
    font-size: 20px;
  }
`;
Section.displayName = 'Section';

const Privacy = () => (
  <Wrapper>
    <Section>
      <StarMode />
    </Section>

    <Section>
      <Password />
    </Section>

    <Section>
      <Email />
    </Section>

    <Section>
      <Suspend />
    </Section>
  </Wrapper>
);

Privacy.propTypes = {
};

Privacy.defaultProps = {
};

export default Privacy;
