import React from 'react';
import PropTypes from 'prop-types';

const GeneralError = ({ error }) => (
  <div>
    <h1>
      GeneralError:
      <span>{error.code}</span>
    </h1>
    {error.message}
  </div>
);

GeneralError.propTypes = {
  error: PropTypes.shape({
    code: PropTypes.number,
    message: PropTypes.string,
  }).isRequired,
};

export default GeneralError;
