import React, { useMemo } from 'react';
import { useRouteMatch } from 'react-router-dom';
import styled from 'styled-components';

import { useTranslation } from 'hooks';

import FloatingButton from 'components/FloatingButton';
import {
  Pencil,
  Link,
  ImageMultiple,
  ViewDashboard,
  Fire,
  Camera,
  PollBox,
} from 'components/Icons';
import TypeSelect from 'components/TypeSelect';

import WarningBoxes from './WarningBoxes';
import LatestHashtags from './LatestHashtags';
import TopFeedContainer from './TopFeedContainer';
import MainFeed from './Main';
import TrendingFeed from './Trending';
import MediaFeed from './Media';
import locales from './i18n';

const Container = styled.div`
  position: relative;

  .emptystate {
    height: 50%;
  }

  .create-icon {
    width: 24px;
    height: 24px;
  }
`;

const newActionStyles = {
  color: 'white',
  style: { width: '24px' },
};

const getCurrentTypeIndex = (match) => {
  switch (match.params.type) {
    case 'trending':
      return 1;
    case 'media':
      return 2;
    case 'feed':
    case undefined:
    default:
      return 0;
  }
};

const Feed = () => {
  const { t } = useTranslation(locales);

  const path = '/:type?';
  const match = useRouteMatch(path);
  const currentTypeIndex = getCurrentTypeIndex(match);

  const typeOptions = useMemo(() => [
    {
      value: 'feed',
      label: (
        <>
          <ViewDashboard className="icon" color="#999" />
          <span>{t('Feed')}</span>
        </>
      ),
      to: '/',
    },
    {
      value: 'trending',
      label: (
        <>
          <Fire className="icon" color="#999" />
          <span>{t('Trending')}</span>
        </>
      ),
      to: '/trending',
    },
    {
      value: 'media',
      label: (
        <>
          <Camera className="icon" color="#999" />
          <span>{t('Last photos')}</span>
        </>
      ),
      to: '/media',
    },
  ], [t]);

  return (
    <Container>
      <WarningBoxes />

      <TopFeedContainer>
        <TypeSelect
          value={typeOptions[currentTypeIndex]}
          options={typeOptions}
        />
        <LatestHashtags />
      </TopFeedContainer>

      {currentTypeIndex === 0 && <MainFeed />}
      {currentTypeIndex === 1 && <TrendingFeed />}
      {currentTypeIndex === 2 && <MediaFeed />}

      <FloatingButton
        actions={[
          { text: t('Text'), to: '/new/text', icon: <Pencil {...newActionStyles} /> },
          { text: t('URL (image/gif/link)'), to: '/new/url', icon: <Link {...newActionStyles} /> },
          { text: t('Photos'), to: '/new/photos', icon: <ImageMultiple {...newActionStyles} /> },
          { text: t('Poll'), to: '/new/poll', icon: <PollBox className="create-icon" {...newActionStyles} /> },
        ]}
      />
    </Container>
  );
};

export default Feed;
