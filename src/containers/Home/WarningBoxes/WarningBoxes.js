import React from 'react';
import styled from 'styled-components';
import { useSelector } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';
import * as invoiceSelectors from 'state/invoices/selectors';

import Unconfirmed from './Unconfirmed';
import Presentation from './Presentation';
import UnpaidInvoices from './UnpaidInvoices';

const WarningBoxesWrapper = styled.div`
  margin: 16px;
`;
WarningBoxesWrapper.displayName = 'WarningBoxesWrapper';

const WarningBoxes = () => {
  const isEmailConfirmed = useSelector(authSelectors.selectIsEmailConfirmed);
  const presentation = useSelector(authSelectors.getOnboardingPresentation);
  const unpaidInvoicesCount = useSelector(invoiceSelectors.unpaidInvoicesCount);

  if (!presentation && isEmailConfirmed && !unpaidInvoicesCount) return null;

  return (
    <WarningBoxesWrapper>
      {!isEmailConfirmed && <Unconfirmed />}
      {isEmailConfirmed && presentation && <Presentation />}
      {isEmailConfirmed && !presentation && unpaidInvoicesCount > 0 && <UnpaidInvoices />}
    </WarningBoxesWrapper>
  );
};

WarningBoxes.propTypes = {
};

WarningBoxes.defaultProps = {
};

export default WarningBoxes;
