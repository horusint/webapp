import styled from 'styled-components';

const Container = styled.div`
  padding: 20px;
  overflow-y: auto;
  overflow-x: hidden;
`;

export default Container;
