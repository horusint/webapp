import React, { useState, useCallback } from 'react';
import styled from 'styled-components';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import loadImage from 'blueimp-load-image';
import { useDispatch, useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';
import * as appActions from 'state/app/actions';
import * as channelsActions from 'state/channels/actions';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';

import Avatar from 'components/ChannelAvatar';
import { MoreActions, Lock } from 'components/Icons';
import Menu, { Item as MenuItem } from 'components/Menu';
import FileSelector from 'components/FileSelector';
import Modal from 'components/Modal';
import Button from 'components/Button';
import MainHeader, { Action } from 'components/Header';

import Online from './Online';
import Members from './Members';
import ShowParticipantsButton from './ShowParticipantsButton';
import AvatarModal from './Modals/AvatarModal';
import EditModal from './Modals/EditModal';
import BotsModal from './Modals/BotsModal';
import DeleteModal from './Modals/DeleteModal';
import BansModal from './Modals/BansModal';
import AddBanModal from './Modals/AddBanModal';
import InvitesModal from './Modals/InvitesModal';
import AddInviteModal from './Modals/AddInviteModal';
import AutorespondersModal from './Modals/AutorespondersModal';
import { CHANNEL_PRIVACIES } from '../../../constants';
import locales from '../i18n';

const Info = styled.div`
  font-size: 12px;
  color: ${colors.grey};
  margin-top: 2px;

  @media(max-width: 767px) {
    color: ${colors.borderRed};

    svg path {
      fill: ${colors.borderRed};
    }
  }
`;

const Header = ({ channelId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const history = useHistory();

  const isOwner = useSelector(state => channelSelectors.isOwnerOfChannel(state, channelId));
  const isMod = useSelector(state => channelSelectors.isModOfChannel(state, channelId));
  const channelName = useSelector(state => channelSelectors.getName(state, channelId));
  const channelAvatar = useSelector(state => channelSelectors.getAvatar(state, channelId));
  const channelPrivacy = useSelector(state => channelSelectors.getPrivacy(state, channelId));

  const [localAvatar, setLocalAvatar] = useState(null);
  const [parting, setParting] = useState(false);
  const [showingPartConfirmModal, setShowingPartConfirmModal] = useState(false);
  const [showingActions, setShowingActions] = useState(false);
  const [showingEditModal, setShowingEditModal] = useState(false);
  const [showingBotsModal, setShowingBotsModal] = useState(false);
  const [showingDeleteModal, setShowingDeleteModal] = useState(false);
  const [showingBansModal, setShowingBansModal] = useState(false);
  const [showingAddBanModal, setShowingAddBanModal] = useState(false);
  const [showingInvitesModal, setShowingInvitesModal] = useState(false);
  const [showingAddInviteModal, setShowingAddInviteModal] = useState(false);
  const [showingAutorespondersModal, setShowingAutorespondersModal] = useState(false);

  const showActions = () => setShowingActions(true);
  const hideActions = () => setShowingActions(false);
  const showPartConfirmModal = useCallback(() => {
    hideActions();
    setShowingPartConfirmModal(true);
  }, []);
  const hidePartConfirmModal = () => setShowingPartConfirmModal(false);

  const showEditModal = useCallback(() => {
    hideActions();
    setShowingEditModal(true);
  }, []);
  const hideEditModal = () => setShowingEditModal(false);

  const showBotsModal = useCallback(() => {
    hideActions();
    setShowingBotsModal(true);
  }, []);
  const hideBotsModal = () => setShowingBotsModal(false);

  const showDeleteModal = () => {
    hideActions();
    setShowingDeleteModal(true);
  };
  const hideDeleteModal = () => setShowingDeleteModal(false);

  const showBansModal = useCallback(() => {
    hideActions();
    setShowingBansModal(true);
  }, []);
  const hideBansModal = () => setShowingBansModal(false);

  const showAddBanModal = useCallback(() => {
    hideActions();
    setShowingAddBanModal(true);
  }, []);
  const hideAddBanModal = () => setShowingAddBanModal(false);

  const showInvitesModal = useCallback(() => {
    hideActions();
    setShowingInvitesModal(true);
  }, []);
  const hideInvitesModal = () => setShowingInvitesModal(false);

  const showAddInviteModal = useCallback(() => {
    hideActions();
    setShowingAddInviteModal(true);
  }, []);
  const hideAddInviteModal = () => setShowingAddInviteModal(false);

  const showAutorespondersModal = useCallback(() => {
    hideActions();
    setShowingAutorespondersModal(true);
  }, []);
  const hideAutorespondersModal = () => setShowingAutorespondersModal(false);

  const part = async () => {
    try {
      setParting(true);
      history.push('/chat');
      await dispatch(channelsActions.part(channelId));
      dispatch(appActions.addToast(t('You left the channel')));
    } catch (error) {
      dispatch(appActions.addError(error));
      setParting(false);
      hidePartConfirmModal();
    }
  };

  const onAvatarFileChange = (e) => {
    loadImage(
      e.target.files[0],
      (img) => {
        const base64data = img.toDataURL('image/jpeg');
        setLocalAvatar({
          src: base64data,
          img,
        });
      },
      {
        maxWidth: 600, maxHeight: 700, orientation: true, canvas: true,
      },
    );
  };
  const closeAvatarModal = () => setLocalAvatar(null);

  const close = useCallback(() => {
    history.push('/chat');
  }, [history]);

  const renderAvatar = useCallback(() => (
    <Avatar image={channelAvatar} />
  ), [channelAvatar]);

  const renderInfo = useCallback(() => (
    <Info>
      <Members channelId={channelId} />
      <Online channelId={channelId} />
    </Info>
  ), [channelId]);

  const renderActions = useCallback(() => (
    <>
      <ShowParticipantsButton />
      <Action onClick={showActions} pressed={showingActions}>
        <MoreActions />
      </Action>
      <Menu open={showingActions} onClose={hideActions} top="50px">
        {isOwner && (
          <>
            <MenuItem>
              <FileSelector onChange={onAvatarFileChange}>
                <span>{t('Change avatar')}</span>
              </FileSelector>
            </MenuItem>
            <MenuItem onClick={showEditModal}>
              {t('Edit channel')}
            </MenuItem>
            <MenuItem onClick={showBotsModal}>
              {t('Handle Bots')}
            </MenuItem>
          </>
        )}
        {isMod && (
          <>
            <MenuItem onClick={showAddBanModal}>
              {t('Add ban')}
            </MenuItem>
            <MenuItem onClick={showInvitesModal}>
              {t('Invites')}
            </MenuItem>
            <MenuItem onClick={showAutorespondersModal}>
              {t('Autoresponders')}
            </MenuItem>
          </>
        )}
        <MenuItem onClick={showBansModal}>
          {t('View bans')}
        </MenuItem>
        <MenuItem onClick={showPartConfirmModal}>
          {t('Part channel')}
        </MenuItem>
      </Menu>
    </>
  ), [
    isMod, isOwner, showAddBanModal, showBansModal, showEditModal,
    showPartConfirmModal, showingActions, t, showInvitesModal,
    showAutorespondersModal, showBotsModal,
  ]);

  const renderTitle = useCallback(() => (
    <>
      {channelPrivacy === CHANNEL_PRIVACIES.PRIVATE && <Lock color="#AAA" />}
      <span>{channelName}</span>
    </>
  ), [channelPrivacy, channelName]);

  return (
    <>
      <MainHeader
        renderTitle={renderTitle}
        renderAvatar={renderAvatar}
        renderInfo={renderInfo}
        renderActions={renderActions}
        onBackClick={close}
      />

      {showingPartConfirmModal && (
        <Modal
          title={t('Part channel')}
          onCancel={hidePartConfirmModal}
          actions={[
            <Button key="remove-thread" onClick={part} loading={parting}>{t('global:Confirm')}</Button>,
          ]}
        >
          {t('Are you sure you want to leave this channel?')}
        </Modal>
      )}

      {showingEditModal && (
        <EditModal channelId={channelId} close={hideEditModal} openDelete={showDeleteModal} />
      )}

      {showingBotsModal && (
        <BotsModal channelId={channelId} close={hideBotsModal} />
      )}

      {showingDeleteModal && (
        <DeleteModal channelId={channelId} close={hideDeleteModal} />
      )}

      {showingBansModal && (
        <BansModal channelId={channelId} close={hideBansModal} />
      )}

      {showingAddBanModal && (
        <AddBanModal channelId={channelId} close={hideAddBanModal} />
      )}

      {showingInvitesModal && (
        <InvitesModal
          channelId={channelId}
          close={hideInvitesModal}
          openAddInvite={showAddInviteModal}
        />
      )}

      {showingAddInviteModal && (
        <AddInviteModal channelId={channelId} close={hideAddInviteModal} />
      )}

      {showingAutorespondersModal && (
        <AutorespondersModal channelId={channelId} close={hideAutorespondersModal} />
      )}

      {!!localAvatar && (
        <AvatarModal
          channelId={channelId}
          image={localAvatar}
          close={closeAvatarModal}
        />
      )}
    </>
  );
};

Header.propTypes = {
  channelId: PropTypes.string.isRequired,
};

export default Header;
