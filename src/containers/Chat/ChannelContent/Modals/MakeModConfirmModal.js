import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';

import * as channelActions from 'state/channels/actions';
import * as appActions from 'state/app/actions';

import { useTranslation } from 'hooks';

import Modal from 'components/Modal';
import Button from 'components/Button';

import { CHANNEL_ROLES } from '../../../../constants';
import locales from '../../i18n';

const MakeModConfirmModal = ({ user, channelId, close }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);

  const [makingMod, setMakingMod] = useState(false);

  const makeMod = async () => {
    try {
      setMakingMod(true);
      await dispatch(channelActions.changeRole(channelId, user.id, CHANNEL_ROLES.MOD));
      dispatch(appActions.addToast(t('User is now mod', { context: user.pronoun })));
      setMakingMod(false);
      close();
    } catch (error) {
      dispatch(appActions.addError(error));
      setMakingMod(false);
      close();
    }
  };

  return (
    <Modal
      title={t('Make mod', { context: user.pronoun })}
      onCancel={close}
      actions={[
        <Button key={`makemod-${user.id}`} onClick={makeMod} loading={makingMod}>{t('global:Confirm')}</Button>,
      ]}
    >
      {t('Are you sure you want to make {{user}} a mod of this channel?', { user: user.displayname })}
    </Modal>
  );
};

MakeModConfirmModal.propTypes = {
  user: PropTypes.shape({
    id: PropTypes.number.isRequired,
    displayname: PropTypes.string.isRequired,
    pronoun: PropTypes.string,
  }).isRequired,
  channelId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired,
};

export default MakeModConfirmModal;
