import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';

import * as channelSelectors from 'state/channels/selectors';

import colors from 'utils/css/colors';
import { useTranslation } from 'hooks';

import { AccessPoint } from 'components/Icons';
import InfoItem from 'components/InfoItem';

import locales from '../i18n';

const Online = ({ channelId }) => {
  const { t } = useTranslation(locales);
  const online = useSelector(state => channelSelectors.onlineCount(state, channelId));

  return (
    <InfoItem title={t('Online')}>
      <AccessPoint color={colors.grey} />
      {online}
    </InfoItem>
  );
};

Online.propTypes = {
  channelId: PropTypes.string.isRequired,
};

export default Online;
