import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { useSelector, shallowEqual } from 'react-redux';

import * as authSelectors from 'state/auth/selectors';

import { useTranslation } from 'hooks';

import EmptyState from 'components/EmptyState';

import Bubble from '../Content/Bubble';
import locales from '../i18n';

const Request = styled.div`
  border-top: 1px solid #ccc;
  margin-top: 30px;
  padding: 20px;
  color: black;
  display: flex;
  flex-direction: column;
  width: auto !important;

  div {
    width: auto;
  }
`;

const WaitingForApproval = ({ message }) => {
  const { t } = useTranslation(locales);
  const me = useSelector(authSelectors.selectMe, shallowEqual);

  return (
    <EmptyState
      title={t('Waiting for approval')}
      subtitle={(
        <>
          {t('You sent a request for chatting with this person but there was not approved yet.')}
          <Request>
            <Bubble author={me}>{message}</Bubble>
          </Request>
        </>
      )}
    />
  );
};

WaitingForApproval.propTypes = {
  message: PropTypes.string.isRequired,
};

export default WaitingForApproval;
