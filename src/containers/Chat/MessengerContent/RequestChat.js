import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Trans } from 'react-i18next';
import { useDispatch } from 'react-redux';

import * as appActions from 'state/app/actions';
import * as messengerActions from 'state/messengers/actions';

import { useTranslation, useInputValue, useOnMount } from 'hooks';

import Button from 'components/Button';
import EmptyState from 'components/EmptyState';

import OnboardingModal from './OnboardingModal';
import locales from '../i18n';

const Textarea = styled.textarea`
  flex: 1;
  outline: none;
  resize: none;
  margin: 60px 0 20px;
  width: 100%;
  height: 100px;
  padding: 10px;
  font-size: 15px;
  border: 1px solid #eee;
  box-sizing: border-box;
`;

const RequestChat = ({ id, participant }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation(locales);
  const requestEl = useRef(null);

  const [loading, setLoading] = useState(false);
  const requestMessage = useInputValue('');

  useOnMount(() => {
    requestEl.current.focus();
  });

  const sendRequest = async () => {
    try {
      setLoading(true);
      await dispatch(messengerActions.request(id, requestMessage.value));
    } catch (error) {
      if (error.response.data.errorCode === 400) dispatch(appActions.addError(t('Message can\'t be empty')));
      else dispatch(appActions.addError(error));

      setLoading(false);
    }
  };

  return (
    <>
      <EmptyState
        title={t('Request for chat!')}
        subtitle={(
          <Trans t={t} i18nKey="chat.request" ns="chatDashboard" tOptions={{ context: participant.pronoun }}>
            You need to send a request to this person to start a conversation.
            <br />
            You can send a message explaining why you want to chat with them, but be mindful:
            you have one shot only!
            <br />
            <Textarea ref={requestEl} placeholder={t('Type a message')} {...requestMessage} />
            <br />

            Remember the three basic rules to establish contact:
            {' '}
            Show consideration! Show respect! Show interest!

            <br />
            <br />

            <Button
              loading={loading}
              disabled={!requestMessage.value.length}
              onClick={sendRequest}
            >
              Send
            </Button>
          </Trans>
        )}
      />

      {/* Modals */}
      <OnboardingModal />
    </>
  );
};

RequestChat.propTypes = {
  id: PropTypes.string.isRequired,
  participant: PropTypes.shape({
    pronoun: PropTypes.string.isRequired,
  }).isRequired,
};

export default RequestChat;
