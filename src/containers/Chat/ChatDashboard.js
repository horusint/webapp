import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector, batch } from 'react-redux';
import { useParams } from 'react-router-dom';

import { useTitle, useTranslation, useHotKeys } from 'hooks';
import * as authSelectors from 'state/auth/selectors';
import * as channelSelectors from 'state/channels/selectors';
import * as messengerSelectors from 'state/messengers/selectors';
import * as messengerActions from 'state/messengers/actions';
import * as channelActions from 'state/channels/actions';
import * as appActions from 'state/app/actions';

import Loading from 'components/Loading';
import ErrorState from 'components/ErrorState';
import Layout from 'components/Layout';

import List from './List';
import Content from './DashboardContent';
import QuickSearchModal from './QuickSearchModal';
import locales from './i18n';

const ChatDashboard = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const { t } = useTranslation(locales);

  const userIsLoggedIn = useSelector(authSelectors.loggedIn);
  const isChannelsInitialized = useSelector(channelSelectors.isInitialized);
  const isMessengerInitialized = useSelector(messengerSelectors.isInitialized);
  const messengerHasFailed = useSelector(messengerSelectors.hasFailed);

  const [showingQuickSearchModal, setShowingQuickSearchModal] = useState(false);

  useEffect(() => {
    if (!params.chatId || !userIsLoggedIn) {
      dispatch(appActions.uiLeftColumn(true));
    } else {
      dispatch(appActions.uiLeftColumn(false));
    }
  }, [params.chatId, dispatch, userIsLoggedIn]);

  const showQuickSearchModal = () => setShowingQuickSearchModal(true);
  const hideQuickSearchModal = () => setShowingQuickSearchModal(false);

  useHotKeys([{ key: 'k', metaKey: true }], showQuickSearchModal);
  useHotKeys([{ key: 'Escape', shiftKey: true }], () => {
    batch(() => {
      dispatch(messengerActions.markAllAsRead());
      dispatch(channelActions.markAllAsRead());
    });
  });

  useTitle(t('global:Chat'));

  if (userIsLoggedIn && (!isMessengerInitialized || !isChannelsInitialized)) return <Loading />;
  if (messengerHasFailed) return <ErrorState message={t('error.fetch')} />;

  return (
    <>
      <Layout columns={userIsLoggedIn ? 2 : 1}>
        {userIsLoggedIn && <List />}
        <Content />
      </Layout>
      {showingQuickSearchModal && <QuickSearchModal close={hideQuickSearchModal} />}
    </>
  );
};

ChatDashboard.propTypes = {
};

export default ChatDashboard;
