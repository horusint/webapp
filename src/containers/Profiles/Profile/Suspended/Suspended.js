import React from 'react';

import { useTranslation } from 'hooks';

import EmptyState from 'components/EmptyState';
import Button from 'components/Button';

import locales from '../../i18n';

const Suspended = () => {
  const { t } = useTranslation(locales);

  return (
    <EmptyState
      uiLeftColumn
      title={t('This account has been suspended')}
    >
      <Button to="/">{t('Go Home')}</Button>
    </EmptyState>
  );
};

Suspended.propTypes = {
};

Suspended.defaultProps = {
};

export default Suspended;
