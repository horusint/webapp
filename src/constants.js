export const SITE_URL = 'https://mazmo.net';
export const API_URL = process.env.REACT_APP_API_URL || 'https://production.mazmoapi.net';
export const EMAILS = {
  MAIN: 'hello@mazmo.net',
  CONFIRMATIONS: 'confirmations@support.mazmo.net',
  SALES: 'ads@mazmo.net',
};

export const DEFAULT_TITLE = 'Mazmo';
export const POST_TITLE = ` - ${DEFAULT_TITLE}`;

export const GIPHY_API_KEY = 'mvFSeyGlQEzhaIR9GZ0SrKLVj0MJLDMU';

export const GA_TRACKING_ID = 'UA-28359298-1';
export const GMAPS_API_KEY = 'AIzaSyDOfi4bcnukLFyfJIto_jHdNNY-xkJz8ik';

export const META_DESCRIPTION = 'Red social de sexualidad y BDSM libre. Chat BDSM, Foros sado, masoquismo, fetish';
export const META_IMAGE = `${SITE_URL}/images/share.jpeg`;

export const FEED_TEMP_PATH = 'https://prod.mazmocdn.com/feed/temp';

export const THREADS_LIST_LIMIT = 30;
export const REPLIES_LIST_LIMIT = 30;
export const PUBLICATIONS_LIST_LIMIT = 30;
export const GUEST_MAX_THREADS = 10;

export const COMMUNITY_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
};

export const CHANNEL_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
};

export const PUBLICATION_PRIVACIES = {
  PUBLIC: 'PUBLIC',
  CONTACTS: 'CONTACTS',
  LISTS: 'LISTS',
};

// ORDER MATTTERS!! - Upper levels has privileges to its belows
export const MEMBERSHIP_ROLES = {
  OWNER: 'OWNER',
  ADMIN: 'ADMIN',
  MODERATOR: 'MODERATOR',
  USER: 'USER',
};

export const CHANNEL_ROLES = {
  OWNER: 'OWNER',
  MOD: 'MOD',
  USER: 'USER',
};

export const GENDERS = {
  MALE: {
    label: 'MALE',
    defaultPronoun: 'MALE',
  },
  FEMALE: {
    label: 'FEMALE',
    defaultPronoun: 'FEMALE',
  },
  CROSSDRESSER: {
    label: 'CROSSDRESSER',
    defaultPronoun: 'NEUTRAL',
  },
  TRAVESTI: {
    label: 'TRAVESTI',
    defaultPronoun: 'NEUTRAL',
  },
  TRANS: {
    label: 'TRANS',
    defaultPronoun: 'NEUTRAL',
  },
  INTERSEX: {
    label: 'INTERSEX',
    defaultPronoun: 'NEUTRAL',
  },
  UNDEFINED: {
    label: 'UNDEFINED',
    defaultPronoun: 'NEUTRAL',
  },
  WOMAN_CIS: {
    label: 'WOMAN_CIS',
    defaultPronoun: 'FEMALE',
  },
  MALE_CIS: {
    label: 'MALE_CIS',
    defaultPronoun: 'FEMALE',
  },
  ANDROGYNOUS: {
    label: 'ANDROGYNOUS',
    defaultPronoun: 'NEUTRAL',
  },
  MALE_TRANS: {
    label: 'MALE_TRANS',
    defaultPronoun: 'MALE',
  },
  FEMALE_TRANS: {
    label: 'FEMALE_TRANS',
    defaultPronoun: 'NEUTRAL',
  },
  TRANSEXUAL: {
    label: 'TRANSEXUAL',
    defaultPronoun: 'NEUTRAL',
  },
  TRANSGENDER: {
    label: 'TRANSGENDER',
    defaultPronoun: 'NEUTRAL',
  },
  FEMALE_TRANSGENDER: {
    label: 'FEMALE_TRANSGENDER',
    defaultPronoun: 'FEMALE',
  },
  MALE_TRANSGENDER: {
    label: 'MALE_TRANSGENDER',
    defaultPronoun: 'MALE',
  },
  QUEER: {
    label: 'QUEER',
    defaultPronoun: 'NEUTRAL',
  },
  NEUTRAL: {
    label: 'NEUTRAL',
    defaultPronoun: 'NEUTRAL',
  },
  NOT_BINARY: {
    label: 'NOT_BINARY',
    defaultPronoun: 'NEUTRAL',
  },
};

export const USERS_PRONOUNS = {
  MALE: 'MALE',
  FEMALE: 'FEMALE',
  NEUTRAL: 'NEUTRAL',
};

export const RELATIONSHIP_TYPES = {
  DOM: 'DOM',
  MASTER: 'MASTER',
  TUTOR: 'TUTOR',
  SPANKER: 'SPANKER',
  OWNER: 'OWNER',
  SWITCHER: 'SWITCHER',
  ALPHA: 'ALPHA',
  SUB: 'SUB',
  SLAVE: 'SLAVE',
  APPRENTICE: 'APPRENTICE',
  BITCH: 'BITCH',
  SPANKEE: 'SPANKEE',
  PET: 'PET',
  CO_SUB: 'CO_SUB',
  GAME_PARTNER: 'GAME_PARTNER',
  INTERN_SUB: 'INTERN_SUB',
  DADDY: 'DADDY',
  LITTLEBOY: 'LITTLEBOY',
  TOY: 'TOY',
};

export const SOCKET_ACTIONS = {
  COMMUNITIES: {
    COMMUNITY_CREATED: 'community:created',
    COMMUNITY_EDITED: 'community:edited',
    COMMUNITY_DELETED: 'community:deleted',

    MEMBERSHIP_CREATED: 'community:membership:created',
    MEMBERSHIP_APPROVED: 'community:membership:approved',
    MEMBERSHIP_DELETED: 'community:membership:deleted',

    THREAD_CREATED: 'community:thread:created',
    THREAD_EDITED: 'community:thread:edited',
    THREAD_DELETED: 'community:thread:deleted',
    THREAD_RESTORED: 'community:thread:restored',

    REPLY_CREATED: 'community:reply:created',
    REPLY_EDITED: 'community:reply:edited',
    REPLY_DELETED: 'community:reply:deleted',
    REPLY_RESTORED: 'community:reply:restored',

    COMMUNITY_JOINED: 'community:joined',
    COMMUNITY_LEFT: 'community:left',
  },

  MESSENGERS: {
    DM_CREATED: 'messengers:directmessage:created',
    DM_UPDATED: 'messengers:directmessage:updated',
    DM_REMOVED: 'messengers:directmessage:removed',
    MESSENGER_UPDATED: 'messengers:messenger:updated',
    MESSENGER_TYPING: 'messengers:messenger:typing',
  },

  CHANNELS: {
    CM_CREATED: 'chat:channelmessage:created',
    CM_UPDATED: 'chat:channelmessage:updated',
    CM_REMOVED: 'chat:channelmessage:removed',
    CHANNEL_UPDATED: 'chat:channel:updated',
    CHANNEL_DELETED: 'chat:channel:deleted',
    CHANNEL_TYPING: 'chat:channel:typing',
  },

  ALERTS: {
    ALERT_CREATED: 'alerts:created',
    ALERT_UPDATED: 'alerts:updated',
  },

  USERS: {
    ONLINE_LIST: 'users:online:list',
    ONLINE: 'users:online:new',
    OFFLINE: 'users:offline:new',
  },

  FEED: {
    NEW_PUBLICATION_IN_FEED: 'feed:publication:added',
    PUBLICATION_UPDATED: 'feed:publication:updated',
    PUBLICATION_REMOVED: 'feed:publication:removed',
    POLL_UPDATED: 'feed:poll:udpdated',
  },

  EVENTS: {
    RSVP_CREATED: 'events:rsvp:created',
    RSVP_REMOVED: 'events:rsvp:removed',
  },
};

export const APP_NAMES = {
  web: 'mazmo-webapp',
};

export const SW = {
  NEW_AUTH: 'notifications/newAuth',
  NEW_THREADS: 'notifications/newThreads',
  NEW_REPLIES: 'notifications/newReplies',
};

export const ALERT_TYPES = {
  COMMUNITY_INVITE_CREATED: 'COMMUNITY_INVITE_CREATED',
  REACTION_REPLY_CREATED: 'REACTION_REPLY_CREATED',
  REACTION_THREAD_CREATED: 'REACTION_THREAD_CREATED',
  COMMUNITIES_THREAD_MENTION: 'COMMUNITIES_THREAD_MENTION',
  FEED_PUBLICATION_VOTE: 'FEED_PUBLICATION_VOTE',

  SADES_TRANSACTION: 'SADES_TRANSACTION',
  SADES_ASSIGNMENT: 'SADES_ASSIGNMENT',
  FOLLOW_CREATED: 'FOLLOW_CREATED',
  KNOW_CREATED: 'KNOW_CREATED',
  COMMENT_CREATED: 'COMMENT_CREATED',

  REACTION_COMMENT_CREATED: 'REACTION_COMMENT_CREATED',
  REACTION_PUBLICATION_CREATED: 'REACTION_PUBLICATION_CREATED',
  RELATIONSHIP_REQUESTED: 'RELATIONSHIP_REQUESTED',
  RELATIONSHIP_APPROVED: 'RELATIONSHIP_APPROVED',
  RELATIONSHIP_REJECTED: 'RELATIONSHIP_REJECTED',
  RELATIONSHIP_REMOVED: 'RELATIONSHIP_REMOVED',

  MESSENGER_APPROVED: 'MESSENGER_APPROVED',
  MESSENGER_REQUESTED: 'MESSENGER_REQUESTED',
  CHAT_CHANNEL_DELETED: 'CHAT_CHANNEL_DELETED',
  CHANNELINVITATION_CREATED: 'CHANNELINVITATION_CREATED',

  FEED_PUBLICATION_REACTED: 'FEED_PUBLICATION_REACTED',
  FEED_COMMENT_REACTED: 'FEED_COMMENT_REACTED',
  FEED_COMMENT_CREATED: 'FEED_COMMENT_CREATED',
  FEED_PUBLICATION_MENTION: 'FEED_PUBLICATION_MENTION',

  RSVP_CREATED: 'RSVP_CREATED',
  RSVP_BANNED: 'RSVP_BANNED',

  // Legacy
  PAGE_RECOMMENDED: 'PAGE_RECOMMENDED',
  NEW_KNOWER: 'NEW_KNOWER',
  PUBLICATION_COMMENTS: 'PUBLICATION_COMMENTS',
  SADES_TRANSFERED: 'SADES_TRANSFERED',
  NEW_FOLLOWER: 'NEW_FOLLOWER',
  PUBLICATION_REACTIONS: 'PUBLICATION_REACTIONS',
};

export const CHANNEL_MESSAGES_TYPES = {
  MESSAGE: 'MESSAGE',
  JOIN: 'JOIN',
  PART: 'PART',
  BAN: 'BAN',
  UNBAN: 'UNBAN',
};

export const PUBLICATION_TYPES = {
  TEXT: 'TEXT',
  MEDIA: 'MEDIA',
  GIF: 'GIF',
  LINK: 'LINK',
  POLL: 'POLL',
};

export const CM_AUTHOR_TYPES = {
  USER: 'USER',
  BOT: 'BOT',
  AUTORESPONDER: 'AUTORESPONDER',
};

export const REPORT_REASONS = {
  SPAM: 'SPAM',
  HACKED: 'HACKED',
  FALSE_IDENTITY: 'FALSE_IDENTITY',
  HATE: 'HATE',
};

export const USERTAGS = {
  // Orientation
  HETEROSEXUAL: 'HETEROSEXUAL',
  HOMOSEXUAL: 'HOMOSEXUAL',
  BISEXUAL: 'BISEXUAL',
  PANSEXUAL: 'PANSEXUAL',
  HETEROFLEXIBLE: 'HETEROFLEXIBLE',
  HOMOFLEXIBLE: 'HOMOFLEXIBLE',
  ASEXUAL: 'ASEXUAL',
  GAY: 'GAY',
  LESBIAN: 'LESBIAN',
  DYKE: 'DYKE',
  FLUCTUENT: 'FLUCTUENT',
  QUEER: 'QUEER',
  FAG: 'FAG',

  // Relationality
  MONOGAMOUS: 'MONOGAMOUS',
  POLIGAMOUS: 'POLIGAMOUS',
  NON_MONOGAMIST: 'NON_MONOGAMIST',
  MONOFLEX: 'MONOFLEX',
  POLYAMOROUS: 'POLYAMOROUS',
  SWINGER: 'SWINGER',
  UNICORN: 'UNICORN',
  ADULTERER: 'ADULTERER',
  CASTITY: 'CASTITY',

  // Status
  IN_A_RELATIONSHIP: 'IN_A_RELATIONSHIP',
  SINGLE: 'SINGLE',
  IN_SEARCH: 'IN_SEARCH',
  NOT_IN_SEARCH: 'NOT_IN_SEARCH',
  AVAILABLE: 'AVAILABLE',
  SEEKING_INFO: 'SEEKING_INFO',
  DND: 'DND',
  UNDER_APPROVAL: 'UNDER_APPROVAL',
  UNDER_CASTITY: 'UNDER_CASTITY',
  UNDER_TRAINING: 'UNDER_TRAINING',
  UNDER_PENITENCE: 'UNDER_PENITENCE',
  PUNISHED: 'PUNISHED',
  TRAINING: 'TRAINING',
  HUNTING: 'HUNTING',

  // Vincularity
  SAPIOSEXUAL: 'SAPIOSEXUAL',
  DEMISEXUAL: 'DEMISEXUAL',
  GREY_ASEXUAL: 'GREY_ASEXUAL',
  AROMANTIC: 'AROMANTIC',
  HEDONIST: 'HEDONIST',

  // Clan
  SPANKO: 'SPANKO',
  FEMDOM: 'FEMDOM',
  MALEDOM: 'MALEDOM',
  PROTOCOLAR: 'PROTOCOLAR',
  TWENTIFOURSEVEN: '24/7',
  FINDOM: 'FINDOM',
  DS: 'D/S',
  SM: 'S&M',
  SHIBARI: 'SHIBARI',
  KINBAKU: 'KINBAKU',
  LEATHER: 'LEATHER',
  TANTRA: 'TANTRA',
  PODOPHYLLUM: 'PODOPHYLLUM',
  BEAR: 'BEAR',

  // EPE role
  VANILLA: 'VANILLA',
  TOP: 'TOP',
  BOTTOM: 'BOTTOM',
  SWITCH: 'SWITCH',
  FETISHIST: 'FETISHIST',

  // Preference role
  DOMINANT: 'DOMINANT',
  SUBMISIVE: 'SUBMISIVE',
  SPANKER: 'SPANKER',
  SPANKEE: 'SPANKEE',
  SADIST: 'SADIST',
  MASOCHIST: 'MASOCHIST',
  SADOMASOCHIST: 'SADOMASOCHIST',
  CURIOUS: 'CURIOUS',
  LIBERTINE: 'LIBERTINE',
  KINKSTER: 'KINKSTER',

  // Detailed Role
  MASTER: 'MASTER',
  SLAVE: 'SLAVE',
  ALPHA: 'ALPHA',
  GOR_KAJIR: 'GOR_KAJIR',
  MAID: 'MAID',
  PET: 'PET',
  TOY: 'TOY',
  THING: 'THING',
  RIGGER: 'RIGGER',
  BUNNY_ROPE: 'BUNNY_ROPE',
  ROLEPLAY: 'ROLEPLAY',
  BRAT: 'BRAT',
  LITTLE: 'LITTLE',
  GIVER: 'GIVER',
  SUGAR_GIVER: 'SUGAR_GIVER',
  SUGARBABY: 'SUGARBABY',
  CUCKOLDRY: 'CUCKOLDRY',
  CELIBATE: 'CELIBATE',
  TAMER: 'TAMER',
  PRIMAL: 'PRIMAL',
  ABDL: 'ABDL',
};

export const POLL_COLORS = {
  RED: '#ED4D3D',
  BLACK: '#000000',
  PINK: '#E91E63',
  PURPLE: '#9C27B0',
  VIOLET: '#673AB7',
  BLUE: '#3F51B5',
  NAVY_BLUE: '#2196F3',
  LIGHT_BLUE: '#03A9F4',
  TURQUOISE: '#00BCD4',
  AQUA: '#009688',
  GREEN: '#4CAF50',
  LIGHT_GREEN: '#8BC34A',
  OLIVE: '#CDDC39',
  YELLOW: '#FFEB3B',
  DARK_YELLOW: '#FFC107',
  ORANGE: '#FF9800',
  DARK_ORANGE: '#FF5722',
  BROWN: '#795548',
  GREY: '#9E9E9E',
  BLUE_GREY: '#607D8B',
};

export const BADGES = {
  ORGANIZER: { value: 10 },
  CONTRIBUTOR: { value: 10 },
  VINYL: { value: 100 },
};
