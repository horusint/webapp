import { useEffect } from 'react';

import { META_IMAGE } from '../constants';

const useMetaImage = (content = META_IMAGE) => {
  useEffect(() => {
    document.querySelector('meta[property="og:image"]').content = content;
    document.querySelector('meta[name="twitter:image"]').content = content;

    return () => {
      document.querySelector('meta[property="og:image"]').content = META_IMAGE;
      document.querySelector('meta[name="twitter:image"]').content = META_IMAGE;
    };
  }, [content]);
};

export default useMetaImage;
