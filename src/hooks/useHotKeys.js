/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';

const useHotKeys = (keys, cb, el = window) => {
  const [position, setPosition] = useState(0);

  useEffect(() => {
    const keydown = (e) => {
      const currentPos = keys[position];
      if (
        e.key === currentPos.key
        && (!currentPos.metaKey || e.metaKey)
        && (!currentPos.shiftKey || e.shiftKey)
      ) {
        if (position === (keys.length - 1)) {
          cb();
          setPosition(0);
        } else {
          setPosition(pos => pos + 1);
        }
      } else if (position !== 0) {
        setPosition(0);
      }
    };

    el.addEventListener('keydown', keydown);
    return () => el.removeEventListener('keydown', keydown);
  }, [position]);
};

export default useHotKeys;
