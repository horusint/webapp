import { useState, useCallback } from 'react';

function useInputValue(initialValue) {
  const [value, setValue] = useState(initialValue);
  const onChange = useCallback((event) => {
    setValue(event.target.value);
  }, []);
  const change = useCallback((val) => {
    setValue(val);
  }, []);

  return {
    value,
    onChange,
    change,
  };
}

export default useInputValue;
