import { useState, useCallback } from 'react';
import { useDebouncedCallback } from 'use-debounce';

import Api from 'state/api';

const useSearchUsers = () => {
  const [value, setValue] = useState('');
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState([]);

  const search = useCallback(async (content) => {
    if (content) {
      try {
        const { data } = await Api.req.get(`/users/search/${content}`);
        setResults(data);
        setLoading(false);
      } catch (error) {
        setLoading(false);
      }
    }
  }, []);

  const [debouncedCallback] = useDebouncedCallback(search, 1000);

  const contentChange = useCallback((event) => {
    const content = event.target.value;
    setValue(content);
    setResults([]);

    if (content.length > 2) {
      setLoading(!!content);
      debouncedCallback(content);
    }
  }, [debouncedCallback]);

  const reset = useCallback(() => {
    setResults([]);
    setValue('');
  }, []);

  return [
    {
      value,
      loading,
      contentChange,
      hasResults: (results.length > 0),
    },
    results,
    reset,
  ];
};

export default useSearchUsers;
