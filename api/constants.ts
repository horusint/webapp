export const API_URI = 'https://production.mazmoapi.net';
export const DEFAULT_TITLE = 'Comunidades BDSM, Chat Sado - Mazmo';
export const DEFAULT_IMAGE = '/images/share.jpeg';
export const DEFAULT_DESCRIPTION = 'La red social de sexualidad libre más grande del mundo hispanoparlante. Foros, comunidades y chats sobre BDSM, sadismo, masoquismo, bondage, spanking, fetichismo y más';
