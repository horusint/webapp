import { NowRequest, NowResponse, NowRequestQuery } from '@vercel/node';
import axios from 'axios';
import fs from 'fs';

import { API_URI } from './constants';
import hydrateHTML from './hydrateHTML';

const getIndex = async (proto: string, host: string) => {
  if (process.env.NODE_ENV === 'development') {
    const data = fs.readFileSync(`${__dirname}/../public/index.html`, 'utf8');
    return data;
  }

  const { data } = await axios.get(`${proto}://${host}/index.html`);
  return data;
};

const getChannel = async ({ channelId }: NowRequestQuery) => {
  const { data } = await axios.get(`${API_URI}/chat/channels/${channelId}`)
  return data;
};

export default async function(req: NowRequest, res: NowResponse) {
  try {
    const promises = [
      getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host)),
      getChannel(req.query),
    ];

    const [indexHTML, channel] = await Promise.all(promises);

    const newTitle = `Chat ${channel.name} - Mazmo`;
    const filenameEncoded = encodeURI(`Chat **${channel.name}**`);
    const newOgImage = `https://og-image.mazmo.net/${filenameEncoded}.jpeg?theme=dark&md=1&fontSize=75px&images=https%3A%2F%2Fmazmo.net%2Fimages%2Flogodark.png&heights=150`;
    const newDescription = channel.description;

    const hydratedHTML = hydrateHTML(indexHTML, newTitle, newDescription, newOgImage);

    res.send(hydratedHTML);
  } catch (error) {
    const indexHTML = await getIndex(String(req.headers['x-forwarded-proto'] || 'http'), String(req.headers.host));
    res.send(indexHTML);
  }
};
