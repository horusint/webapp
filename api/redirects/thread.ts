import { NowRequest, NowResponse, NowRequestQuery } from '@vercel/node';
import axios from 'axios';

import { API_URI } from '../constants';

const getThread = async ({ communitySlug, threadSlug }: NowRequestQuery) => {
  const { data } = await axios.get(`${API_URI}/communities/${communitySlug}/threads/${threadSlug}?oldSlug=1`);
  return data;
};

export default async function(req: NowRequest, res: NowResponse) {
  try {
    const thread = await getThread(req.query);
    return res.redirect(301, `https://mazmo.net/+${req.query.communitySlug}/${thread.slug}`);
  } catch (error) {
    return res.redirect(301, 'https://mazmo.net');
  }
};
